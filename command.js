#!/usr/bin/env node

const program = require('commander');
const inquirer = require('inquirer');
const fs = require('fs');
const { config } = require('./config');
const projectFolder = config.deployerPath + '/config/projects';
let handler = require('./handler');

const workspacesQuestion = [
    {
      type : 'list',
      name : 'workspace',
      message : 'What is your workspace?',
      choices: config.workspaces
    }
];

const options = {
    'composer' : [
        {
            type : 'list',
            name : 'composer',
            message : 'What is your composer action?',
            choices: [
                'install',
                'update'
            ]
        }
    ]
};

program
.version('0.0.1')
.description('Fast deployer');

inquirer.prompt(workspacesQuestion).then(answerWorspace => {
    
    fs.readdirSync(projectFolder + '/' + answerWorspace.workspace).forEach(file => {
        handler.setProject(projectFolder + '/' + answerWorspace.workspace + '/' + file);
    });
    let ProjectsQuestion = [
        {
          type : 'checkbox',
          name : 'project',
          message : 'What is your project deployment needed?',
          choices: handler.getParsedProject()
        }
    ];
    
    const stateQuestion = [
        {
          type : 'list',
          name : 'state',
          message : 'What is your state?',
          choices: config.states[answerWorspace.workspace]
        }
    ];
    inquirer.prompt(stateQuestion).then(answerState => {
        inquirer.prompt(ProjectsQuestion).then(answerProjects => {
            inquirer.prompt(options.composer).then(answerComposer => {

                // handler.preExecute();

                let command = handler.makeDeployCommand({
                    workspace: answerWorspace.workspace,
                    projects: answerProjects.project,
                    composer: answerComposer.composer,
                    state: answerState.state
                });
                
                handler.runCommand(command);
            });
        });
    });
});

return 0;

if (!process.argv.slice(2).length || !/[arudl]/.test(process.argv.slice(2))) {
    program.outputHelp();
    process.exit();
}

program.parse(process.argv);