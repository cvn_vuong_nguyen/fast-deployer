const fs = require("fs");
const { config } = require('./config');
const projectFolder = config.deployerPath + '/config/projects';
let cmd = require('node-cmd');

module.exports = {

    project: [],
    workspace: [],    

    setProject: function(file) {
        if (this.isDirectory(file)) {
            this.project.push(file);
        }
    },

    getProject: function() {
        return this.project;
    },

    getParsedProject: function() {
        let data = [];
        this.project.forEach(p => {
            var x = p.match(/(-[\w]+)?(-[\w]+)?(-\w+$)/i);
            data.push(x[0].substring(1));
        });

        return data;
    },

    isDirectory: function(file) {
        try {
            return fs.lstatSync(file).isDirectory()
       } catch(e){
            console.log(e);
       }
    },

    makeDeployCommand: function(options) {
        if (!options || options.projects.length <= 0) {
            console.log('Missing option or project not choose');
            return false;
        }

        let composer = ' --force-composer-install';
        let verbose = ' -vvv';
        let bin = config.deployerPath + '/bin/pegasus-deployer';
        let concurrentlyCommand = './node_modules/.bin/concurrently';
        let alias = '';
        let state = options.state;
        let branch = '';

        if (options.composer === 'update') {
            composer = ' --force-composer-update';
        }
        if (config.alias[options.workspace]) {
            options.workspace = config.alias[options.workspace];
        }

        if (config.branchAlias[state]) {
            branch = ' --branch=' + config.branchAlias[state] + ' --force'
        }
        
        options.projects.forEach(function(project) {
            let command = bin + ' deploy-' + options.workspace + ':' + project  + ' ' + options.state + composer + branch;
            concurrentlyCommand += ` "${command}"`;
        });

        return concurrentlyCommand;
    },

    runCommand: function(command) {
        let data_line = '';
        
        const a = cmd.get(
            command,
            function(err, data, stderr){
                console.log(command);
            }
        );
    
        a.stdout.on('data',
            function(data) {
                data_line += data;

                if (data_line[data_line.length-1] == '\n') {
                    console.log(data_line);
                }
            }
        );
    },

    preExecute: function () {
        if (config.preScript && config.preScript.length > 0) {
            config.preScript.forEach(function (script) {

            })
        }
    }
};